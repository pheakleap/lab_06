<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" 
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" 
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <?php
        include("connection.php");
    ?>
    <div>
        <button><a href="create.php">Add</a></button>
    </div>
     <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Phone Number</th>
                <th scope="col">First Name</th>
                <th scope="col">Last Name</th>
                <th scope="col">Url_fb_social</th>
                <th scope="col">Profile picture</th>
            </tr>
        </thead>
        <tbody>
        <?php
             $sql = "SELECT * FROM contact";
             $data = $con->query($sql);    
             if ($data->num_rows>0){
                 while($rows = $data -> fetch_assoc()){
                     $id = $rows['id'];
                     $phone = $rows['phoneNum'];
                     $firstname = $rows['firstname'];
                     $lastname = $rows['lastname'];
                     $url = $rows['url_fb_social'];
                     $profile = $rows['profile_picture'];
                            echo "<tr>
                                    <td>$id</td>
                                    <td>$phone</td>
                                    <td>$firstname</td>
                                    <td>$lastname</td>
                                    <td>$url</td>
                                    <td><img src='$profile' height ='40px'/></td>
                                    <td>
                                        <button type='button' class='btn btn-outline-success'>
                                            <a href='update.php?updateid=$id'>Update</a>
                                        </button>
                                    </td>
                                    <td>
                                        <button type='button' class='btn btn-outline-danger'>
                                            <a href='delete.php?delete=$id'>Delete</a>
                                        </button>
                                    </td>
                                </tr>";
                        }
            }
        ?>
</body>
</html>