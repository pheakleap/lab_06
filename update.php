<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" 
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" 
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <?php
        include("connection.php");
    ?>
     <div class="container">
        <form action="#" method="POST" role="form" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="" class="form-label">Phone Number</label>
            <input type="text" class="form-control" id="" placeholder="Enter your phone number" name="phone">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">First Name</label>
            <input type="text" class="form-control" id="" placeholder="Enter your First Name" name="firstname">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Last Name</label>
            <input type="text" class="form-control" id="" placeholder="Enter your Last Name" name="lastname">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Url facebook socail</label>
            <input type="text" class="form-control" id="" placeholder="Enter your facebook url" name="url">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Profile picture</label>
            <input type="text" class="form-control" id="" placeholder="Enter your profile picture link" name="profile">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-danger" name="submit">Update</button>
        </div>
        </form>
    </div>
    <div>
        <button><a href="read.php">Read</a></button>
    </div>
    <?php
        $id = $_GET['updateid'];
        if(isset($_POST['submit'])){
            $phone = $_POST['phone'];
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $url = $_POST['url'];
            $profile = $_POST['profile'];
            $sql_contact= "UPDATE contact set id='$id', phone='$phone', firstname='$firstname', lastname='$lastname', url_fb_social='$url', profile_picture='$profile' WHERE id=$id";
            $result = mysqli_query($con,$sql_contact);
                if($result){
                    echo "<script>alert('Update Successful')</script>;";
                    header('Location: read.php');
                }
                else{
                    echo "<script>alert('Failed')</script>;";
                }
        }
        $con->close();
    ?>
</body>
</html>