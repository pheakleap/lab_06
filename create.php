<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" 
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
    integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" 
    integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" 
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body>
    <?php
        include 'connection.php';
    ?>
    <div class="container">
        <form action="#" method="POST" role="form" enctype="multipart/form-data">
        <div class="mb-3">
            <label for="" class="form-label">Phone Number</label>
            <input type="tel" class="form-control" id="" placeholder="Enter your Phone Number" name="Phone">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">First Name</label>
            <input type="text" class="form-control" id="" placeholder="Enter your First Name" name="firstname">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Last Name</label>
            <input type="text" class="form-control" id="" placeholder="Enter your Last Name" name="lastname">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Url facebook socail</label>
            <input type="text" class="form-control" id="" placeholder="Enter your facebook url" name="url">
        </div>
        <div class="mb-3">
            <label for="" class="form-label">Profile picture</label>
            <input type="text" class="form-control" id="" placeholder="Enter your profile picture link" name="profile">
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-danger" name="create">Submit</button>
        </div>
        </form>
    </div>
    <div>
        <button><a href="read.php">Read</a></button>
    </div>
    <?php
        if(isset($_POST['create'])){
            $phone = $_POST['phone'];
            $firstname = $_POST['firstname'];
            $lastname = $_POST['lastname'];
            $url = $_POST['url'];
            $profile = $_POST['profile'];
            $sql_contact= "INSERT INTO contact(phoneNum,firstname, lastname, url_fb_social, profile_picture) VALUES (?,?,?,?,?)";
            $stmt_contact = $con-> prepare($sql_contact);
            $stmt_contact->bind_param("issss", $phone, $firstname, $lastname, $url, $profile);
                if($stmt_contact->execute()){
                    echo "<script>alert('Success')</script>;";
                }
                else{
                    echo "<script>alert('Failed')</script>;";
                }
        }
        $con->close();
    ?>
</body>
</html>